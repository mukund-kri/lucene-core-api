package learn.lucene;

import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;

public class CourtesyTitleExample {
	
	/**
	 * The code that runs this example
	 * 
	 * @return void
	 */
	public void runExample() {
		
		try {
			System.out.println("Writing analyzer/tokenizer/token-filter ... ");
			
			Reader reader = new StringReader(
					"Mrs quick fox jumped over the Mr brown  dog");
			
			Analyzer myAnalyzer = new CourtesyTitleAnalyzer();
			Utils.printTokenStream(myAnalyzer, reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
