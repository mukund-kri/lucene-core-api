package learn.lucene;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;

/**
 * Run a basic sentence through the simplest possible custom analyzer
 * that just uses the WhiteSpaceTokenizer. 
 * 
 * @author mukund
 */
public class ExploreCustomAnalyzers {

	/**
	 * The code that runs this example
	 * 
	 * @return void
	 */
	public void runExample() {
		
		try {
			System.out.println("Exploring custom analyzers ... ");
			
			Reader reader = new StringReader(
					"The quick fox jumped over the lazy brown  dog");
			
			Analyzer myAnalyzer = new FirstCustomAnalyzer();
			Utils.printTokenStream(myAnalyzer, reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
