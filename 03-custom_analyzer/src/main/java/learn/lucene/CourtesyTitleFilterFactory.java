package learn.lucene;

import java.util.Map;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.util.TokenFilterFactory;


public class CourtesyTitleFilterFactory extends TokenFilterFactory {

    public CourtesyTitleFilterFactory(Map<String, String> args) {
        super(args);
        if(!args.isEmpty()) {
            throw new IllegalArgumentException("Unknown parameters " + args);
        }
    }

    @Override
    public TokenStream create(TokenStream input) {
        return new CourtesyTitleFilter(input);
    }

}
