package learn.lucene;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

public class CourtesyTitleFilter extends TokenFilter {
	
    private Map<String, String> synonyms = new HashMap<String, String>();
    private CharTermAttribute termAttr;
    private TypeAttribute typeAttr;
    private OffsetAttribute offsetAttr;

    public CourtesyTitleFilter(TokenStream input) {
        super(input);
        termAttr = addAttribute(CharTermAttribute.class);
        typeAttr = addAttribute(TypeAttribute.class);
        offsetAttr = addAttribute(OffsetAttribute.class);
        synonyms.put("Dr", "doctor");
        synonyms.put("Mrs", "miss");
        synonyms.put("Mr", "mister");
    }
	
    @Override
    public boolean incrementToken() throws IOException {

        System.out.println(typeAttr.type());
        System.out.println(offsetAttr.toString());
        if(!input.incrementToken()) {
            return false;
        }
        String text = termAttr.toString();
        if(synonyms.containsKey(text)) {
            this.termAttr.setEmpty().append(synonyms.get(text));
        }
        return true;
	}

}
