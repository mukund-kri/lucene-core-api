package learn.lucene;

import java.io.IOException;
import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;

public class Utils {
	public static void printTokenStream(Analyzer analyzer, Reader reader) throws IOException {
        TokenStream stream = analyzer.tokenStream("text", reader);

        OffsetAttribute offsetAtt = stream.addAttribute(OffsetAttribute.class);
        CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);

        stream.reset();

        while(stream.incrementToken()) {
            String token = termAtt.toString();
            System.out.println("[" + token + "]");

            System.out.println("  Token starting offset :: " + offsetAtt.startOffset());
            System.out.println("  Token ending offset   :: " + offsetAtt.endOffset());
            System.out.println("");
        }

	}
}
