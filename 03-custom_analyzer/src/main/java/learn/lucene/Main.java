package learn.lucene;

class Main {

    public static void main(String[] args) {
        System.out.println("Experiment with building custom analyzers in lucene");
        
        // ExploreCustomAnalyzers eca = new ExploreCustomAnalyzers();
        // eca.runExample();
        
        CourtesyTitleExample cte = new CourtesyTitleExample();
        cte.runExample();
    }

}
