package learn.lucene;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;


public class CourtesyTitleAnalyzer extends Analyzer {

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
		
        Tokenizer simpleTokenizer = new StandardTokenizer();
        TokenFilter courtesyFilter = new CourtesyTitleFilter(simpleTokenizer);
        return new TokenStreamComponents(simpleTokenizer, courtesyFilter);
    }
}
