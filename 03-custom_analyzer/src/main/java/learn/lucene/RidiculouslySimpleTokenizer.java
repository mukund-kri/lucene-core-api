package learn.lucene;

import org.apache.lucene.analysis.util.CharTokenizer;

/**
 * Example from the packt book.
 * A very simple tokenizer. All the non space characters are part of 
 * tokens and any type of spaces are delimiters.
 * 
 * @author mukund
 */
public class RidiculouslySimpleTokenizer extends CharTokenizer {

	
	@Override
	protected boolean isTokenChar(int c) {
		return !Character.isSpaceChar(c);
	}

}
