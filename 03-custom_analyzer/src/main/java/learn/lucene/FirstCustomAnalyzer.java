package learn.lucene;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;


public class FirstCustomAnalyzer extends Analyzer {
	
	@Override
	protected TokenStreamComponents createComponents(String field) {
		Tokenizer tokenizer = new WhitespaceTokenizer();
		return new TokenStreamComponents(tokenizer);
	}
}
