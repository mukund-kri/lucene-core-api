import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"

  val luceneVersion = "7.3.1"
  lazy val luceneCore        = "org.apache.lucene" % "lucene-core"             % luceneVersion
  lazy val luceneAnalyzers   = "org.apache.lucene" % "lucene-analyzers-common" % luceneVersion
  lazy val luceneQueryparser = "org.apache.lucene" % "lucene-queryparser"      % luceneVersion
}
