package learn.lucene

import org.apache.lucene.analysis.core.WhitespaceAnalyzer
import org.apache.lucene.index.{IndexWriter, IndexWriterConfig, IndexReader, DirectoryReader}
import org.apache.lucene.store.RAMDirectory
import org.apache.lucene.search.{IndexSearcher}
import org.apache.lucene.queryparser.classic.QueryParser

import org.apache.lucene.document.{Document, Field, TextField}



object Main extends App {

  try {

    // CREATING AND WRITING TO THE INDEX

    val analyzer = new WhitespaceAnalyzer()
    val directory = new RAMDirectory()
    val writer = new IndexWriter(directory, new IndexWriterConfig(analyzer))

    // Create a simple document
    val doc = new Document()
    val field = new TextField("text1", "This is the string that gets stored", Field.Store.YES)
    doc.add(field)

    // Add the document into the index
    writer.addDocument(doc)
    writer.close

    // READING FROM THE INDEX PART

    // load up the index in read mode
    val searcher = new IndexSearcher(DirectoryReader.open(directory))

    // create and run the query
    val qp = new QueryParser("text1", analyzer)
    val query = qp.parse("string")
    val docs = searcher.search(query, 5)
    val hits = docs.scoreDocs

    println("First result ...")
    val d = searcher.doc(hits(0).doc)
    println(d)


  } catch {
    case e: Throwable => e.printStackTrace()
  }
}

