package learn.lucene

import org.scalatest._

class MainSpec extends FlatSpec with Matchers {
  "The Hello object" should "say hello" in {
    "hello" shouldEqual "hello"
  }
}
