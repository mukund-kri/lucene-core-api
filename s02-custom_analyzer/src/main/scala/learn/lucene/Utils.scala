package learn.lucene


import java.io.{Reader, IOException}

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.tokenattributes.{OffsetAttribute, CharTermAttribute}

/** A set of utility function to make it easy to write lucene example
  * easier to write. It's very java like for now, till I figure out how to
  * make all this a lot me ideomatic scala */
object Utils {

  @throws[IOException]
  def printTokenStream(analyzer: Analyzer, reader: Reader, fieldToAnalyze: String) {

    val stream = analyzer.tokenStream(fieldToAnalyze, reader)
    val offsetAttr = stream.addAttribute(classOf[OffsetAttribute])
    val termAttr   = stream.addAttribute(classOf[CharTermAttribute])
    stream.reset()

    while(stream.incrementToken()) {
      val token = termAttr.toString()
      println("[" + token + "]")
    }

  }
}
