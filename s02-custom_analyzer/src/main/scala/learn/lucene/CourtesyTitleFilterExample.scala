
package learn.lucene

import java.io.StringReader


object CourtesyTitleFilterExample {

  def run {
    println("Running Courtesy title filter example ... ")

    try {
      val reader = new StringReader("Mrs quick fox jumped over Mr brown dog")
      val myAnalyzer = new CourtesyTitleAnalyzer()
      Utils.printTokenStream(myAnalyzer, reader, "text")
    } catch {
      case e: Throwable => e.printStackTrace()
    }
  }

}
