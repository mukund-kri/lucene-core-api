package learn.lucene

import java.util.{Map => JMap}

import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.util.TokenFilterFactory


class CourtesyTitleFilterFactory(args: JMap[String, String]) extends TokenFilterFactory(args) {

  override def create(input: TokenStream): TokenStream = {
    new CourtesyTitleFilter(input)
  }
}
