package learn.lucene

import org.apache.lucene.analysis.{Analyzer, TokenFilter}
import org.apache.lucene.analysis.core.WhitespaceTokenizer


class CourtesyTitleAnalyzer extends Analyzer {

  override def createComponents(fileName: String): Analyzer.TokenStreamComponents = {
    val simpleTokenizer = new WhitespaceTokenizer()
    val courtesyTitleFilter: TokenFilter = new CourtesyTitleFilter(simpleTokenizer)
    return new Analyzer.TokenStreamComponents(simpleTokenizer, courtesyTitleFilter)
  }

}
