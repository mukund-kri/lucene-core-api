package learn.lucene

import java.io.IOException
import scala.collection.mutable.HashMap

import org.apache.lucene.analysis.{TokenFilter, TokenStream}
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;


class CourtesyTitleFilter(input: TokenStream) extends TokenFilter(input) {

  val synonyms: HashMap[String, String] = HashMap(
    "Dr"  -> "Doctor1",
    "Mrs" -> "missus1",
    "Mr"  -> "mister1"
  )
  var termAttr = addAttribute(classOf[CharTermAttribute])

  @throws[IOException]
  override def incrementToken(): Boolean = {
    if(!input.incrementToken()) {
      false
    } else {
      val tokenText = this.termAttr.toString()
      this.termAttr.setEmpty()
      .append(synonyms.getOrElse(tokenText, tokenText))
      true
    }
  }


}
