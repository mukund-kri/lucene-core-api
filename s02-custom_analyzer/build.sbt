import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "learn.lucene",
      scalaVersion := "2.12.6",
      version      := "0.0.1-SNAPSHOT"
    )),
    name := "custom_analyzer",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      luceneCore,
      luceneAnalyzers,
      luceneQueryparser
    )
  )
