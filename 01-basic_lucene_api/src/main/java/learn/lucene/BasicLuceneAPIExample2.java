package learn.lucene;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.DirectoryReader;

import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.ScoreDoc;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;


/**
 * An extension of the BasicLuceneAPIExample, but with these changes.
 *  - Index to the file system instead of RAM as in the previous case
 *  - The documents a series of lines in a text files
 *  - The code is better structured into methods here.
 *
 * @author   Mukund Krishnamurthy
 * @version  0.0.1
 * @since    2018.06.21
 */
class BasicLuceneAPIExample2 {

    Analyzer analyzer;
    Directory directory;
    IndexWriter writer;
    IndexSearcher searcher;
    String source;

    Analyzer ana2 = new SimpleAnalyzer();
    
    /**
     * Setup the index on the file system and create an IndexWriter and a
     * IndexSearcher.
     */
    BasicLuceneAPIExample2(String indexDirectory,
                            String sentenceSourceFile) {
        try {
            this.analyzer = new WhitespaceAnalyzer();
            this.directory = FSDirectory.open(Paths.get(indexDirectory));
            this.source = sentenceSourceFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Create the IndexWriter
    private void createWriter() throws IOException {
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        this.writer = new IndexWriter(this.directory, config);
    }

    // Create the IndexSearcher
    private void createSearcher() throws IOException {
        IndexReader reader = DirectoryReader.open(this.directory);
        this.searcher = new IndexSearcher(reader);
    }


    /**
     * Search for a term on the IndexSearcher and print out the matching
     * documents
     *
     * @params searchTerm The term to search on the index
     *
     * @return void
     */
    public void search(String searchTerm) throws IOException, ParseException {
        this.createSearcher();

        QueryParser qp = new QueryParser("text", this.analyzer);
        Query query = qp.parse(searchTerm);
        int hitsPerPage = 10;
        TopDocs docs = this.searcher.search(query, hitsPerPage);
        ScoreDoc[] hits = docs.scoreDocs;
        long end = Math.min(docs.totalHits, hitsPerPage);
        for(int i=0; i<end; i++) {
            Document d = searcher.doc(hits[i].doc);
            System.out.println(d);
        }
        
    }

    private Document makeDocument(String sentence) {
        Document doc = new Document();
        Field text = new TextField("text", sentence, Field.Store.YES);
        doc.add(text);
        return doc;
    }

    public void indexFromFile(String fileName) throws IOException  {
        System.out.println("Indexing sentences .... ");
        this.createWriter();
        Stream<String> sentences = Files.lines(Paths.get(fileName));
        sentences.forEach((sentence) -> {
            Document doc = this.makeDocument(sentence);
            try {
                writer.addDocument(doc);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        this.writer.close();
    }

    /**
     * Run this example
     *
     * @return void
     */
    public void runExample() {
        try {
            this.indexFromFile(this.source);
            this.search("join");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
