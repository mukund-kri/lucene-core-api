package learn.lucene;

class Main {

    public static void main(String[] args) {
        System.out.println("Basic Lucene examples ... ");

        // BasicLuceneAPIExample basics = new BasicLuceneAPIExample();
        // basics.runExample();

        BasicLuceneAPIExample2 basics2 =
            new BasicLuceneAPIExample2("/data/lucene/index_basic_example",
                                       "./sentences.txt");
        basics2.runExample();
    }

}
