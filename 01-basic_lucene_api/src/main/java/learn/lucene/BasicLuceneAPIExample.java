package learn.lucene;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.DirectoryReader;

import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.ScoreDoc;


import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;


/**
 * The most basic example of full cycle lucene example. This one show how
 * to index and then query a lucene document.
 *
 * @author   Mukund Krishnamurthy
 * @version  0.0.1
 * @since    2018.06.21
 */
class BasicLuceneAPIExample {

    /**
     * Run the full cycle of indexing and querying
     *
     * @return void
     */
    public void runExample() {
        System.out.println("Running basic lucene API Example ... ");

        try {
            Analyzer analyzer = new WhitespaceAnalyzer();
            Directory directory = new RAMDirectory();
            IndexWriterConfig config = new IndexWriterConfig(analyzer);
            IndexWriter writer = new IndexWriter(directory, config);

            // Create a new "Lucene Document"
            Document doc = new Document();
            String text = "This is the string to index";
            doc.add(new TextField("text1", text, Field.Store.YES));

            // Now put the document into the index
            writer.addDocument(doc);
            writer.close();

            // Now reading / searching from the index part
            IndexReader reader = DirectoryReader.open(directory);
            IndexSearcher searcher = new IndexSearcher(reader);

            // Create the query
            QueryParser qp = new QueryParser("text1", analyzer);
            Query query = qp.parse("string");
            int hitsPerPage = 5;
            TopDocs docs = searcher.search(query, hitsPerPage);
            ScoreDoc[] hits = docs.scoreDocs;
            long end = Math.min(docs.totalHits, hitsPerPage);

            for(int i=0; i<end; i++) {
                Document d = searcher.doc(hits[i].doc);
                System.out.println(d);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
