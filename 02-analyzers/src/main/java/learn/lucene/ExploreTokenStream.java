package learn.lucene;

import java.io.Reader;
import java.io.StringReader;
import java.io.IOException;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;


class ExploreTokenStream {

    public void runExample() throws IOException {
        System.out.println("Token stream example ... ");

        Reader reader =
            new StringReader("A character stream whose source is a string.");
        Analyzer analyzer = new SimpleAnalyzer();
        TokenStream stream = analyzer.tokenStream("text", reader);

        OffsetAttribute offsetAtt = stream.addAttribute(OffsetAttribute.class);
        CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);

        stream.reset();

        while(stream.incrementToken()) {
            String token = termAtt.toString();
            System.out.println("[" + token + "]");

            System.out.println("  Token starting offset :: " + offsetAtt.startOffset());
            System.out.println("  Token ending offset   :: " + offsetAtt.endOffset());
            System.out.println("");
        }

    }
}
