package learn.lucene;

import java.io.IOException;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/**
 * The entry point where all the example code in this project is run.
 *
 * @author   Mukund Krishnamurthy
 * @version  0.0.1
 * @since    2018.06.24
 */
class Main {

    @Argument(required=true)
    private String exampleName;


    public static void main(String[] args) throws Exception {

        try {
            new Main().runMain(args);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Parse the command line and run the relevant example 
     *
     * @params args String[] the command line arguments that is passed to the
     *              class main()
     *
     * @return void
     */
    public void runMain(String[] args) throws IOException, CmdLineException {

        CmdLineParser parser = new CmdLineParser(this);
        parser.parseArgument(args);

        System.out.println("Lucene :: Analyzers, Tokenizers and Filters ... ");

        switch(this.exampleName.toLowerCase()) {

        case "token-stream":
            // Explore the "Token Stream", the data is passed from tokenizers
            // and filters in a chain
            ExploreTokenStream ets = new ExploreTokenStream();
            ets.runExample();
            break;

        case "per-field":
            ExplorePerFieldAnalysis epfa = new ExplorePerFieldAnalysis();
            epfa.runExample();
            break;
            
        default:
            System.out.println("Unknown example");

        }

    }

}
